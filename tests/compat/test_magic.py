from unittest import TestCase, mock

from aletheia.compat.magic import get_mimetype


class MagicTestCase(TestCase):
    """
    These are tough to test without explicitly installing the different
    modules, so these mocks are the best I could do.
    """

    @mock.patch("aletheia.compat.magic.magic")
    @mock.patch("aletheia.compat.magic.hasattr", return_value=True)
    def test_get_mimetype_python_magic(self, _, m):
        m.from_file = mock.Mock(return_value="test")
        self.assertEqual(get_mimetype(""), "test")

    @mock.patch("aletheia.compat.magic.magic")
    @mock.patch("aletheia.compat.magic.hasattr", return_value=False)
    def test_get_mimetype_file_magic(self, _, m):
        m.detect_from_filename = mock.Mock(
            return_value=mock.Mock(mime_type="test")
        )
        self.assertEqual(get_mimetype(""), "test")
